import { ExpenseService } from './expense.service';
import { Body, Controller, Get, Logger, Post, Query } from '@nestjs/common';

@Controller('expense')
export class ExpenseController {
    constructor(private expenseService: ExpenseService) {}

    @Post()
    async addExpenseDetail(
        @Body('year') year: number,
        @Body('month') month: number,
        @Body('user') user: string,
        @Body('cost') cost: number,
        @Body('content') content: string,
    ) {
        const isSuccess = await this.expenseService.insertExpenseDetail(
            month,
            year,
            user,
            cost,
            content,
        );
        return { success: isSuccess };
    }

    @Get()
    getExpense(@Query('month') month: number, @Query('year') year: number) {
        if (month && year) {
            return this.expenseService.findExpenseByMonth(month, year);
        }
        return this.expenseService.getAllExpense();
    }
}
