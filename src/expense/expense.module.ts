import { ExpenseDetailSchema } from './schemas/expenseDetail.schema';
import { Module, Logger } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ExpenseSchema } from './schemas/expense.schema';
import { ExpenseController } from './expense.controller';
import { ExpenseService } from './expense.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'Expense', schema: ExpenseSchema },
            { name: 'ExpenseDetail', schema: ExpenseDetailSchema },
        ]),
    ],
    controllers: [ExpenseController],
    providers: [ExpenseService],
})
export class ExpenseModule {}
