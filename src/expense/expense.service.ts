import { ExpenseDetail } from './schemas/expenseDetail.schema';
import { Expense } from './schemas/expense.schema';
import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class ExpenseService {
    constructor(
        @InjectModel('Expense') private expenseModel: Model<Expense>,
        @InjectModel('ExpenseDetail')
        private expenseDetailModel: Model<ExpenseDetail>,
    ) {}

    async insertExpenseDetail(
        month: number,
        year: number,
        user: string,
        cost: number,
        content: string,
    ): Promise<boolean> {
        const newExpenseDetail = new this.expenseDetailModel({
            user,
            cost,
            content,
        });
        const createdExpense = await newExpenseDetail.save();

        const result = await this.expenseModel.updateOne(
            { month, year },
            {
                $push: { expenseDetails: createdExpense },
                $inc: { total: createdExpense.cost },
            },
            { new: true },
        );

        // todo: check result

        return true;
    }

    async getAllExpense(): Promise<Expense[]> {
        const expenses = await this.expenseModel.find().exec();
        return expenses.map(expense => ({
            id: expense.id,
            month: expense.month,
            year: expense.year,
            total: expense.total,
            isPay: expense.isPay,
            expenseDetails: expense.expenseDetails.map(item => ({
                id: item.id,
                user: item.user,
                cost: item.cost,
                content: item.content,
                createdAt: item.createdAt,
            })),
        }));
    }

    async createExpense(month: number, year: number) {
        const newExpense = new this.expenseModel({
            month,
            year,
        });
        const result = await newExpense.save();
        return result.id;
    }

    async findExpenseByMonth(
        month: number,
        year: number,
    ): Promise<Expense | null> {
        const expense = await this.expenseModel.findOne({ month, year }).exec();
        return expense;
    }
}
