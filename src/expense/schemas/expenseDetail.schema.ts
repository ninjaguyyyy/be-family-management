import * as mongoose from 'mongoose';
import { Document } from 'mongoose';

export const ExpenseDetailSchema = new mongoose.Schema(
    {
        user: String,
        cost: Number,
        content: String,
    },
    { timestamps: true },
);

export interface ExpenseDetail extends Document {
    id: string;
    user: string;
    cost: number;
    content: string;
    createdAt: Date;
}
