import { ExpenseDetail } from './expenseDetail.schema';
import * as mongoose from 'mongoose';
import { Document } from 'mongoose';

export const ExpenseSchema = new mongoose.Schema(
    {
        month: Number,
        year: Number,
        total: Number,
        isPay: Boolean,
        expenseDetails: Array,
    },
    { timestamps: true },
);

export interface Expense extends Document {
    id: string;
    month: number;
    year: number;
    total: number;
    isPay: boolean;
    expenseDetails: ExpenseDetail[];
}
