import * as mongoose from 'mongoose';
import { Document } from 'mongoose';

export const UserSchema = new mongoose.Schema({
    name: String,
});

export interface User extends Document {
    id: string;
    name: string;
}
