import { User } from './schemas/user.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class UsersService {
    constructor(@InjectModel('User') private userModel: Model<User>) {}

    async getAllUsers(): Promise<User[]> {
        const users = await this.userModel.find().exec();

        return users.map((user, index) => ({
            id: user.id,
            name: user.name,
        }));
    }
}
