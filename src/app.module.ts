import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ExpenseModule } from './expense/expense.module';
import { UsersModule } from './users/users.module';

@Module({
    imports: [
        MongooseModule.forRoot(
            'mongodb+srv://chungchihuong:3006@cluster0.u5by8.mongodb.net/family-management?retryWrites=true&w=majority',
        ),
        ExpenseModule,
        UsersModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
